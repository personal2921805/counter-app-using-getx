# GetX Flutter Counter App

This is a simple Flutter counter app that demonstrates how to use the GetX package for state management. GetX provides a powerful and reactive way to manage your app's state, making it easy to update your UI when the state changes.

## Getting Started

1. Make sure you have Flutter and Dart installed on your development machine.

2. Clone this repository to your local machine using the following command:

   ```bash
   git clone https://gitlab.com/personal2921805/counter-app-using-getx
   ```

3. Navigate to the project directory:

   ```bash
   cd getx_counter_app
   ```

4. Install the project dependencies:

   ```bash
   flutter pub get
   ```

5. Run the app:

   ```bash
   flutter run
   ```

## Project Structure

The project structure is organized as follows:

- `lib` directory: Contains the main code for the application.
    - `main.dart`: Entry point of the application.
    - `counter_app.dart`: Defines the `CounterApp` widget.
    - `home_page.dart`: Defines the `HomePage` widget.
    - `counter_controller.dart`: Defines the `CounterController` class.

## CounterController

The `CounterController` class is responsible for managing the state of the counter. It extends `GetxController` from the GetX package. It includes the `count` variable and an `increment()` method that increments the count. The `update()` method is used to notify the UI when the count changes.

```dart
class CounterController extends GetxController {
  var count = 0;

  void increment() {
    count++;
    update(); // Notify UI to update
  }
}
```

## HomePage

The `HomePage` widget displays the current count and a floating action button. The `GetBuilder` widget listens to changes in the `CounterController` and updates the UI automatically when the count changes.

```dart
class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final CounterController counterController = Get.put(CounterController());
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          counterController.increment();
        },
        child: const Icon(Icons.add),
      ),
      body: Center(
        child: GetBuilder<CounterController>(
          builder: (controller) {
            return Text(
              '${controller.count}',
              style: const TextStyle(fontSize: 30, fontWeight: FontWeight.w600),
            );
          },
        ),
      ),
    );
  }
}
```

## Running the App

Run the app using the following command:

```bash
flutter run
```

You should see the counter app with a button to increment the count and a text widget displaying the current count. The UI will update automatically when the count changes, thanks to the reactive nature of GetX and the use of `GetBuilder`.

---

Feel free to customize this README template to fit your project's needs. This README provides a basic guide to setting up a counter app using `update()`, `GetxController`, and `GetBuilder` with the GetX package in Flutter.